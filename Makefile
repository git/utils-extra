# SPDX-FileCopyrightText: 2017-2023 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0

include config.mk

all: $(EXE)

.c:
	rm -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -o $@ $< $(LDFLAGS) $(LDSTATIC)

.c.o:
	rm -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -c -o $@ $<

.PHONY: check
check: all
	MALLOC_CHECK_=3 POSIX_ME_HARDER=1 POSIXLY_CORRECT=1 kyua test || (kyua report --verbose --results-filter=broken,failed; false)

.PHONY: lint
lint:
	$(SHELLCHECK) ./configure test_functions.sh ./sh/*
	SHELLCHECK=${SHELLCHECK} ./test-cmd/shellcheck
	${FLAWFINDER} --error-level=4 .
	$(MANDOC) -Tlint -Wunsupp,error,warning $(MAN1)
	$(REUSE) lint

clean:
	rm -fr $(EXE)
	rm -fr ${EXE:=.c.gcov} ${EXE:=.gcda} ${EXE:=.gcno}

install: all
	mkdir -p ${DESTDIR}${BINDIR}/
	cp -p ${EXE} ${DESTDIR}${BINDIR}/
	chown 0:0 ${DESTDIR}${BINDIR}/memsys
	chmod 4755 ${DESTDIR}${BINDIR}/memsys
	mkdir -p ${DESTDIR}${MANDIR}/man1
	cp -p ${MAN1} ${DESTDIR}${MANDIR}/man1

.PHONY: coverage
coverage:
	$(GCOV) -b $(EXE)

C_SOURCES = cmd/*.c
format: $(C_SOURCES)
	clang-format -style=file -assume-filename=.clang-format -i $(C_SOURCES)

cmd/lolcat: cmd/lolcat.c Makefile
	rm -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -o $@ cmd/lolcat.c -lm $(LDFLAGS) $(LDSTATIC)

cmd/xcd: cmd/xcd.c Makefile
	rm -f ${<:=.gcov} ${@:=.gcda} ${@:=.gcno}
	$(CC) -std=c99 $(CFLAGS) -o $@ cmd/xcd.c -lm $(LDFLAGS) $(LDSTATIC)
