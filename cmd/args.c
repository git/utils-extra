// utils-extra: Collection of extra tools for Unixes
// SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
// printf()
#include <stdio.h>

int
main(int argc, char *argv[])
{
	printf("argc: %i\n", argc);

	for(int i = 0; i < argc; i++)
	{
		printf("argv[%d]: \"%s\"\n", i, argv[i]);
	}

	return 0;
}
