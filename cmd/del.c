// utils-extra: Collection of extra tools for Unixes
// SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include <errno.h>  /* errno */
#include <stdio.h>  /* remove() */
#include <string.h> /* strerror() */

int
main(int argc, char *argv[])
{
	for(int i = 1; i < argc; i++)
	{
		if(remove(argv[i]) < 0)
		{
			// TODO: interact on write protection to force deletion
			fprintf(stderr, "del: remove(%s) error: %s\n", argv[i], strerror(errno));
			return 1;
		}
	}

	return 0;
}
