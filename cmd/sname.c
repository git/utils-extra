// utils-extra: Collection of extra tools for Unixes
// SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

// uname(2) isn't in POSIX
#define _GNU_SOURCE
#include <stdio.h>       // printf(), perror()
#include <stdlib.h>      // exit()
#include <sys/utsname.h> // utsname, uname()

void
print_kv(char *key, char *value)
{
	if(printf("%s	%s\n", key, value) < 0)
	{
		perror("sname: printf");
		exit(1);
	}
}

int
main()
{
	struct utsname name;
	if(uname(&name) != 0)
	{
		perror("uname");
		return 1;
	}

	print_kv("machine", name.machine);
	print_kv("nodename", name.nodename);
	print_kv("release", name.release);
	print_kv("sysname", name.sysname);
	print_kv("version", name.version);

	return 0;
}
