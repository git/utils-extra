// utils-extra: Collection of extra tools for Unixes
// SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define FORMAT_M "sizeof(%s) == %d bytes; %d bits; (MIN:MAX) == (%d:%d)\n"

static void
print_size(size_t size, char *type)
{
	int ret = printf("sizeof(%s) == %zd bytes; %zd bits\n", type, size, size * CHAR_BIT);
	if(ret < 0)
	{
		exit(1);
	}
}

int
main(void)
{
	int c, ret;

	ret = printf("CHAR_BIT == %d\n", CHAR_BIT);
	if(ret < 0)
	{
		exit(1);
	}

	c = sizeof(int);
	/* flawfinder: ignore. Not given by user but by a macro */
	ret = printf(FORMAT_M, "int", c, c * CHAR_BIT, INT_MIN, INT_MAX);
	if(ret < 0)
	{
		exit(1);
	}
	c = sizeof(char);
	/* flawfinder: ignore. Not given by user but by a macro */
	ret = printf(FORMAT_M, "char", c, c * CHAR_BIT, CHAR_MIN, CHAR_MAX);
	if(ret < 0)
	{
		exit(1);
	}

	print_size(sizeof(uint8_t), "uint8_t");
	print_size(sizeof(short), "short");
	print_size(sizeof(long), "long");
	print_size(sizeof(long long), "long long");
	print_size(sizeof(float), "float");
	print_size(sizeof(double), "double");
	print_size(sizeof(long double), "long double");
	print_size(sizeof(double long), "double long");
	print_size(sizeof(char[BUFSIZ]), "char[BUFSIZ]");
	print_size(sizeof(char[256]), "char[256]");
	print_size(sizeof(char[32]), "char[32]");
	print_size(sizeof(char[2]), "char[2]");
	print_size(sizeof('a'), "'a'");

	return 0;
}
