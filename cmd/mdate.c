// utils-extra: Collection of extra tools for Unixes
// SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include <stdio.h> /* printf */
#include <time.h>  /* time */

// 3600*24.5
#define cycle 88200

int
main(void)
{
	time_t now      = time(NULL);
	time_t date_now = now / cycle;
	time_t time_now = now % cycle;

	printf("%lX,%05lX\n", date_now, time_now);

	return 0;
}
