// utils-extra: Collection of extra tools for Unixes
// SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
// SPDX-License-Identifier: MPL-2.0

#define _POSIX_C_SOURCE 200809L
#include <fcntl.h>  /* open() */
#include <stdio.h>  /* perror() */
#include <unistd.h> /* close(), write() */

#define SYS_POWER_STATE "/sys/power/state"

int
main(void)
{
	int fd, err = 0;
	char *entry       = "mem";
	size_t entry_size = 3;

	fd = open(SYS_POWER_STATE, O_WRONLY);

	if(fd == -1)
	{
		perror("memsys: open(\"" SYS_POWER_STATE "\")");
		err++;
	}
	else
	{
		if(write(fd, entry, entry_size) < (ssize_t)entry_size)
		{
			perror("memsys: write(fd, \"mem\", 3)");
			err++;
		}
		close(fd);
	}

	return err;
}
