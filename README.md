# utils-extra: Collection of extra tools for Unixes
Copyright 2017-2023 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
SPDX-License-Identifier: MPL-2.0

This is a repository of tools that are typically missing from Unix systems, in a fashion similar to [moreutils](https://joeyh.name/code/moreutils/).

Tested on Linux(musl), FreeBSD, NetBSD, OpenBSD: <https://builds.sr.ht/~lanodan/utils-extra>

## Goals
- Provide simple generic tools which can be composed with existing others
- Code readability and hackability
- Portability
- Efficiency, but not without sacrificing the other goals

## Non-Goals
- Implement commonly available tools, this is [utils-std](https://hacktivis.me/git/utils-std)'s purpose
- Also shouldn't reimplement existing tools, although it may conflict with others from time to time

## Dependencies
- POSIX System
- (optional, test) ATF: <https://github.com/jmmv/atf>
- (optional, test) Kyua: <https://github.com/jmmv/kyua>
- (optional, test) bwrap: <https://github.com/containers/bubblewrap/> For safely overlaying false files on the root filesystem
- (optional, lint) mandoc: <https://mdocml.bsd.lv/> For linting the manual pages
- (optional, lint) shellcheck: <https://www.shellcheck.net/> For linting `./configure` and shell scripts

## Packaging
- The `./configure` script isn't auto*hell based, you can pass it arguments via key-value arguments or environment variables
- You can define the `NO_BWRAP` environment variable to skip the bwrap-based tests which can cause issues in some environments like Gentoo `sandbox`
- If you want statically linked executables, define `LDSTATIC=-static` in the environment
