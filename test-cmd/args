#!/usr/bin/env atf-sh
# SPDX-FileCopyrightText: 2017-2022 Haelwenn (lanodan) Monnier <contact+utils@hacktivis.me>
# SPDX-License-Identifier: MPL-2.0


atf_test_case noargs
noargs_body() {
	atf_check -o 'inline:argc: 1
argv[0]: "../cmd/args"
' ../cmd/args
}

atf_test_case onearg
onearg_body() {
	atf_check -o 'inline:argc: 2
argv[0]: "../cmd/args"
argv[1]: "a"
' ../cmd/args a
}

atf_test_case twoargs
twoargs_body() {
	atf_check -o 'inline:argc: 3
argv[0]: "../cmd/args"
argv[1]: "a"
argv[2]: "b c"
' ../cmd/args a 'b c'
}

atf_test_case options
options_body() {
	atf_check -o 'inline:argc: 5
argv[0]: "../cmd/args"
argv[1]: "-1"
argv[2]: "+2"
argv[3]: "--3"
argv[4]: "/4"
' ../cmd/args -1 +2 --3 /4
}

atf_init_test_cases() {
	cd "$(atf_get_srcdir)" || exit 1
	atf_add_test_case noargs
	atf_add_test_case onearg
	atf_add_test_case twoargs
	atf_add_test_case options
}
